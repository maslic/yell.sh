

# yell.sh
A command line [yellkey][yellkey] URL shortener. [Yellkey][yellkey] is a 
temporary URL shortener, that shortens to a single English word for up to 24 hours.


This program takes the URL from standard input or the first argument (if no
standard input is given). Returns the short URL into standard output.

[yellkey]: https://www.yellkey.com


## --help

```
NAME
  yell - command line url to common word shortener

SYNOPSIS
  yell [URL]

DESCRIPTION
  Shorten URL in standard input to standard output.

  With no standard input, first argument is used.
```


# TODO

 - Take "time" form an argument, 1st if stdin, 2nd if argument, default to 24h


## Dependencies

 - bash
 - curl


## API responses
For reference.

Sucessbul response:

```json
{"errorMessage":"A full, valid URL (e.g. https://www.google.com) must be provided."}
```

Failed response:

```json
{"key":"YOUR_KEY"}
```

