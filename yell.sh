#!/bin/bash

# Get the URL
if test -t 0; then 
	if test -z "$1"; then
		echo "No URLs passed or recognised. See --help."
	fi	
	URL="$1"
else 
	URL="$(cat -)"
fi

# Shorten the URL
case "$URL" in
	'http'*)
		RESPONSE=$(curl -s "https://www.yellkey.com/api/new?url=$URL&time=1440")
		MESSAGE=$(echo "$RESPONSE" | cut -d'"' -f4)
		;;
	'--help'|'-h')
		cat << EOF_help
NAME
	yell - command line url to common word shortener

SYNOPSIS
	yell [URL]

DESCRIPTION
	Shorten URL in standard input to standard output.

	With no standard input, first argument is used.
EOF_help
		exit 0 ;;
	*) echo "Bad input (the URL needs to start with 'http'. See --help." 
		exit 41 ;;
esac

# Give response
case "$RESPONSE" in
	'{"key":'*) echo "https://www.yellkey.com/$MESSAGE"
		exit 0 ;;
	'{"errorMessage"'*) echo "$MESSAGE"
		echo "Use --help or -h for help."
		exit 5 ;;
	*)
		cat << EOF_UnkownError
Unknows error. Here's the response.

$RESPONSE

You cound try reporting this on https://gitlab.com/maslic/yell.sh
EOF_UnkownError
		exit 41 ;;
esac

